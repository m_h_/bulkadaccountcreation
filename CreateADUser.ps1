﻿Function CreateADUser {
    [CmdletBinding()]
        param(
            [parameter(Mandatory = $true)]
            [PSCustomObject] $User
        )

    # Create AD User
    Write-Host "   Creating AD account..." -foreground Green -NoNewLine
    
    New-ADUser `
        -Name $User.Displayname `
        -Office $User.Office.ToUpper() `
        -HomeDrive $User.HomeDrive `
        -HomeDirectory $User.HomeDirectory `
        -DisplayName $User.Displayname `
        -SamAccountName $User.SAM `
        -UserPrincipalName $User.UPN `
        -GivenName $User.UserFirstname `
        -Surname $User.UserLastname `
        -Description $User.Description `
        -AccountPassword (ConvertTo-SecureString $User.Password -AsPlainText -Force) `
        -Path $User.OU `
        -ChangePasswordAtLogon $true `
        –PasswordNeverExpires $false
    
    Start-Sleep -s 5
    
    Set-ADUser $User.SAM -Enabled $true 
    
    Start-Sleep -s 5

    Write-Host "   Done" -foreground Blue
}
