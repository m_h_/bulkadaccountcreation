﻿Function UpdateADUser {
    [CmdletBinding()]
        param(
            [parameter(Mandatory = $true)]
            [PSCustomObject] $UserData
        )
    
    # Update AD User
    Write-Host "   Adding telephone, address, and primary group properties..." -foreground Green -NoNewLine
    
    Start-Sleep -s 20
    
    $Telephone = $User.PhonePrefix + $User.Extension
    
    Get-ADUser $User.SAM | Set-ADObject -Replace @{
        primarygroupid = "REDACTED";
        ipphone = $Extension;
        telephonenumber = $Telephone; 
    }

    Get-ADUser $User.SAM `
        | Set-ADUser `
            -StreetAddress $User.Street `
            -City $User.Location `
            -State $User.State `
            -PostalCode $User.Postcode `
            -Country $User.Country         
               
    Write-Host "   Done" -foreground Blue
}
