﻿Function ModifyExistingUser {
     [CmdletBinding()]
        param(
            [parameter(Mandatory = $true)]
            [PSCustomObject] $User
        )

    # Modify existing AD User
    Write-Host "   Modifying Existing AD account..." -foreground Green -NoNewLine
    
    $GUID = (Get-ADUser $User.SAM).ObjectGUID
        
    #Move to correct OU
    Move-ADObject -Identity $GUID -TargetPath $User.OU
    
    Start-Sleep -s 5
   
    # Update User based on CSV
    Set-ADUser `
        -Identity $User.SAM `
        -Office $User.Office `
        -HomeDrive $User.HomeDrive `
        -HomeDirectory $User.HomeDirectory `
        -DisplayName $User.Displayname `
        -SamAccountName $User.SAM `
        -UserPrincipalName $User.UPN `
        -GivenName $User.UserFirstname `
        -Surname $User.UserLastname `
        -Description $User.Description `
        -ChangePasswordAtLogon $false `
        –PasswordNeverExpires $false
    Start-Sleep -s 5

    # Reset Password as per CSV
    Set-ADAccountPassword -Identity $User.SAM -Reset -NewPassword (ConvertTo-SecureString $User.Password -AsPlainText -Force) 

    Get-ADUser $User.SAM | Set-ADObject -Replace @{ displayname = $User.Displayname }
       
    Set-ADUser $User.SAM -Enabled $true 
   
    Start-Sleep -s 5
   
    Write-Host "   Done" -foreground Blue
}
