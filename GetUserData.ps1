﻿#--- FUNCTIONS ---#
Function GetUserData {
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true)]
        [PSCustomObject] $UserData
    )
   
    # Get user details from CSV object
    Write-Host "Setting up User:" $UserData.SAM -foreground Yellow

    # Create new UserData object and add the data from the CSV file to it
    $User = New-Object -TypeName psobject
    $User | Add-Member -MemberType NoteProperty -Name HomeDrive -Value "G:" 
    $User | Add-Member -MemberType NoteProperty -Name FirstName -Value $UserData.Firstname
    $User | Add-Member -MemberType NoteProperty -Name Lastname -Value $UserData.Lastname 
    $User | Add-Member -MemberType NoteProperty -Name Displayname -Value ($UserData.Lastname + ", " + $UserData.Firstname)
    $User | Add-Member -MemberType NoteProperty -Name OU -Value $UserData.OU  
    $User | Add-Member -MemberType NoteProperty -Name SAM -Value $UserData.SAM
    $User | Add-Member -MemberType NoteProperty -Name Office -Value $UserData.Office 
    $User | Add-Member -MemberType NoteProperty -Name Email -Value ($UserData.Firstname + "." + $UserData.Lastname + "@REDACTED.com.au")    
    $User | Add-Member -MemberType NoteProperty -Name UPN -Value ($UserData.Firstname + '.' + $UserData.LastName + "@REDACTED.com.au")
    $User | Add-Member -MemberType NoteProperty -Name Description -Value $UserData.Description 
    $User | Add-Member -MemberType NoteProperty -Name Password -Value $UserData.Password 
    $User | Add-Member -MemberType NoteProperty -Name Extension -Value $UserData.Extension 
    $User | Add-Member -MemberType NoteProperty -Name Mailbox -Value $UserData.MBX 

    If($User.Office.ToUpper() -eq "SYD") {
        $User | Add-Member -MemberType NoteProperty -Name Directory -Value ("\\REDACTED\Users")
        $User | Add-Member -MemberType NoteProperty -Name HomeDirectory -Value ("\\REDACTED\Users\" + $UserData.SAM)
        $User | Add-Member -MemberType NoteProperty -Name ACLDirectory -Value "\\REDACTED\Users"
    } Else {
        $User | Add-Member -MemberType NoteProperty -Name Directory -Value ("\\" + $User.Office + "fps\Users")
        $User | Add-Member -MemberType NoteProperty -Name HomeDirectory -Value ("\\" + $UserData.Office + "REDACTED\Users\" + $UserData.SAM)
        $User | Add-Member -MemberType NoteProperty -Name ACLDirectory -Value ("\\" + $UserData.Office + "REDACTED\Users")
    }

    # Set Location-specific variables
    Switch($User.Office.ToUpper()) {
        "BNE" { 
            $User | Add-Member -MemberType NoteProperty -Name Location -Value "Brisbane"
            $User | Add-Member -MemberType NoteProperty -Name PhonePrefix -Value "+REDACTED " 
            $User | Add-Member -MemberType NoteProperty -Name Street -Value "REDACTED"
            $User | Add-Member -MemberType NoteProperty -Name City -Value  "REDACTED"
            $User | Add-Member -MemberType NoteProperty -Name State -Value "REDACTED"
            $User | Add-Member -MemberType NoteProperty -Name Postcode -Value "REDACTED"
            $User | Add-Member -MemberType NoteProperty -Name Country -Value "REDACTED"
            $User | Add-Member -MemberType NoteProperty -Name LineURI -Value "tel:+REDACTED"
           }
        "PER" { 
            $User | Add-Member -MemberType NoteProperty -Name Location -Value "Perth"
            $User | Add-Member -MemberType NoteProperty -Name PhonePrefix -Value "+REDACTED " 
            $User | Add-Member -MemberType NoteProperty -Name Street -Value "REDACTED"
            $User | Add-Member -MemberType NoteProperty -Name City -Value  "REDACTED"
            $User | Add-Member -MemberType NoteProperty -Name State -Value "REDACTED"
            $User | Add-Member -MemberType NoteProperty -Name Postcode -Value "REDACTED"
            $User | Add-Member -MemberType NoteProperty -Name Country -Value "REDACTED"
            $User | Add-Member -MemberType NoteProperty -Name LineURI -Value "tel:+REDACTED"
            }
        "MEL" { 
            $User | Add-Member -MemberType NoteProperty -Name Location -Value "Melbourne"
            $User | Add-Member -MemberType NoteProperty -Name PhonePrefix -Value "+REDACTED " 
            $User | Add-Member -MemberType NoteProperty -Name Street -Value "REDACTED"
            $User | Add-Member -MemberType NoteProperty -Name City -Value  "REDACTED"
            $User | Add-Member -MemberType NoteProperty -Name State -Value "REDACTED"
            $User | Add-Member -MemberType NoteProperty -Name Postcode -Value "REDACTED"
            $User | Add-Member -MemberType NoteProperty -Name Country -Value "REDACTED"
            $User | Add-Member -MemberType NoteProperty -Name LineURI -Value "tel:+REDACTED"
            }
        "SYD" {
            $User | Add-Member -MemberType NoteProperty -Name Location -Value "Sydney"
            $User | Add-Member -MemberType NoteProperty -Name PhonePrefix -Value "+REDACTED " 
            $User | Add-Member -MemberType NoteProperty -Name Street -Value "REDACTED"
            $User | Add-Member -MemberType NoteProperty -Name City -Value  "REDACTED"
            $User | Add-Member -MemberType NoteProperty -Name State -Value "REDACTED"
            $User | Add-Member -MemberType NoteProperty -Name Postcode -Value "REDACTED"
            $User | Add-Member -MemberType NoteProperty -Name Country -Value "REDACTED"
            $User | Add-Member -MemberType NoteProperty -Name LineURI -Value "tel:+REDACTED"
            }
       
    }

    Return $User
}
