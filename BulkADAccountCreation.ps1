#--- MAIN ---#
Function BulkADAccountCreation {    

    If($SessionOptions -eq $null) {

        #Import Exchange and AD Modules
        $SessionOptions = New-PSSessionOption –SkipCACheck –SkipCNCheck –SkipRevocationCheck 

        $Session2 = New-PSSession -ConfigurationName Microsoft.Exchange `
            -ConnectionUri http://REDACTED.REDACTED.com.au/Powershell `
            -Authentication Kerberos -sessionOption $sessionOptions

        Import-PsSession $Session2
        Import-Module ActiveDirectory  
        Import-Module SkypeForBusiness
    }

    $UsersCSV = Import-Csv -Path "\\REDACTED\NewUser.csv"

    # Loop through each User in CSV File          
    ForEach ($UserData in $UsersCSV) { 
        $User = GetUserData -User $UserData
    }
        # Check if user already exists in AD 
        Try {
            Get-ADUser -Identity $User.SAM | Out-Null
            $UserExists = $true
        } Catch [Microsoft.ActiveDirectory.Management.ADIdentityResolutionException] {
            "User does not exist."
            $UserExists = $false
        }


        # Check if User already in Active Directory
        If($UserExists) {
            # Modify existing AD User
            ModifyExistingUser -User $User
        } Else {
            # User does not exist in AD and needs creation
            CreateADUser -User $User
        }

        AddADGroups -User $User -WhatIf

        UpdateADUser -User $User -WhatIf
    
        # Enables mailbox in Exchange and enables unified messaging
        ExchangeSetup -User $User -WhatIf
    
        # Currently untested and non-functional
        #SkypeSetup -User $User

    Write-Host "Script completed." -foreground Green

} 
