﻿Function ExchangeSetup {
    [CmdletBinding()]
    param(
        [parameter(Mandatory=$true)]
        [PSCustomObject] $UserData
    )

    # Exchange Settings
    Write-Host "   Enabling mailbox..." -foreground Green -NoNewLine
    
    Start-Sleep -s 30
    
    Enable-Mailbox `
        -Identity $User.SAM `
        -Database $User.Mailbox `
        | Out-Null
    
    Write-Host "   Done" -foreground Blue

    # Enable Unifed Messaging
    Write-Host "   Enabling unified messaging..." -foreground Green -NoNewLine
    
    Start-Sleep -s 30
    
    Enable-UMMailbox `
        -Identity $User.Email `
        -UMMailboxPolicy "REDACTED" `
        -Extensions $User.Extension `
        | Out-Null
    
    Write-Host "   Done" -foreground Blue
}
