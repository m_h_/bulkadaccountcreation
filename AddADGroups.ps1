﻿Function AddADGroups {
    [CmdletBinding()]
        param(
            [parameter(Mandatory = $true)]
            [PSCustomObject] $UserData
        )
        
    # Add Groups
    Write-Host "   Adding Group memberships..." -foreground Green -NoNewLine
    
    Add-ADGroupMember -identity 'app REDACTED' -Members $User.SAM | Out-Null 
    Add-ADGroupMember -identity $User.AppSnapCommsLocation -Members $User.SAM | Out-Null 

    Add-ADGroupMember -identity 'REDACTED1' -Members $User.SAM | Out-Null 
    Add-ADGroupMember -identity 'REDACTED2' -Members $User.SAM | Out-Null 
    Add-ADGroupMember -identity 'REDACTED3' -Members $User.SAM | Out-Null 
    Add-ADGroupMember -identity 'REDACTED4' -Members $User.SAM | Out-Null 

    Write-Host "   Done" -foreground Blue
}
